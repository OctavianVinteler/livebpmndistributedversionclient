import io from 'socket.io-client';
import BpmnViewer from 'bpmn-js';
import {BPMNClient} from './semantics/bpmn-client'
import {SimulationEditor} from './ui/simulationEditor';
import {BPMNUtil} from './util/bpmn-util';
import BpmnModdle from 'bpmn-moddle';
import $ from 'jquery';

var bePackage = require('./ui/be.json');
var clientSemantics;
var render;
var canvas;

function registerFileDrop(container, callback) {
  function handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();

    var files = e.dataTransfer.files;
    var file = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      var xml = e.target.result;
      callback(xml);
    };
    reader.readAsText(file);
  }

  function handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();

    e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  container.addEventListener('dragover', handleDragOver, false);
  container.addEventListener('drop', handleFileSelect, false);
}

function removeMarkers(element, canvas) {
  canvas.removeMarker(element, 'enabled');
  canvas.removeMarker(element, 'executed');
  canvas.removeMarker(element, 'selectable');
  canvas.removeMarker(element, 'blocked');
}

function redefineTaskForm(semantics) {
  var allElements = semantics.currentState.allElements;

  for(let el of allElements) {
    if(semantics.isUserTask(el)) {
      var extension = getExtension(el, 'be:BusinessTask');
      if(!BPMNUtil.testArrayEquality(Object.keys(JSON.parse(extension.activationSchema)),
                                    Object.keys(window.formTemplate))) {
        extension.activationSchema = JSON.stringify(window.formTemplate);
      }
    }
  }
}

function openUserTaskWindow(detail, editor) {
  // Get the activation properties for this particular task
  var extension = getExtension(detail.element, 'be:BusinessTask');
  var activationSchema = JSON.parse(extension.activationSchema);
  // Id the schema was never set, set the default model
  if(JSON.stringify(activationSchema) == JSON.stringify({})) {
    activationSchema = JSON.parse(JSON.stringify(window.formTemplate));
  }
  editor.activateTaskProperties(activationSchema, window.businessDataObject);

  window.activateEditor(detail);
}

function destroyEditors() {
  if(window.flowSequnceEditor) {
    window.flowSequnceEditor.destroy();
  }
  if(window.businessDataObject) {
    window.businessDataObject.destroy();
  }
  if(window.taskSchemaObject) {
    window.taskSchemaObject.destroy();
  }
  if(window.scriptTaskEditor) {
    window.scriptTaskEditor.destroy();
  }
  if(window.temp) {
    window.temp.destroy();
  }
  if(window.messageEventData) {
    window.messageEventData.destroy();
  }
  if(window.messageEventEditor) {
    window.messageEventEditor.destroy();
  }
}

function startSimulation(startButton, defineTaskSchemaButton, clientSemantics, editor) {
  clientSemantics.simulationStarted = true;
  startButton.disabled = true;
  defineTaskSchemaButton.disabled = true;

  var taskSchema = JSON.parse(window.taskSchemaObject.getEditor('root').getValue()['json-representation']);
  var inputSchema = editor.processTaskSchema(taskSchema);
  window.setBusinessEditor(inputSchema);
  window.setMessageEventData(inputSchema);
}

function handleScriptTask(elem, socket) {
  var scriptExt = getExtension(elem, 'be:ScriptTask');
  var scriptString = JSON.parse(scriptExt.script).script;

  if(scriptString) {
    var businessObject = window.businessDataObject.getEditor('root').getValue();
    try {
      eval('businessObject.' + scriptString);
      window.businessDataObject.setValue(businessObject);
      socket.emit('businessObject', {schema: window.businessDataObject.getEditor('root').getValue()});
    } catch (e) {
      console.log('Invalid Script');
    }
  }
}

function handleMessageEvent(elem, editor) {
  var messageExt = getExtension(elem, 'be:MessageEvent');
  var activationSchema = JSON.parse(messageExt.message);
  // if the activation schema is empty, don't show the form
  if(JSON.stringify(activationSchema) != JSON.stringify({})) {
    editor.activateTaskProperties(activationSchema, window.messageEventData);
    window.displayEventMessage(elem);
  }
}

// io.configure(function () {
//   io.set("transports", ["xhr-polling"]);
//   io.set("polling duration", 10);
// });
// var socket = io.connect('https://localhost');
var socket = io.connect('https://distributed-livebpmn-server.herokuapp.com:443', {
    transports: ['websocket']
});
// Property for adding the listeners just the first time
socket.initialization = true;

socket.on('model', data => {
  processDiagram(data);
  socket.emit('diagram');
});

var uploadDiagramButton = document.getElementById('upload-diagram');
$(uploadDiagramButton).unbind();
$(uploadDiagramButton).bind('click', function() {
  var fileInput = document.getElementById('file-upload');
  $(fileInput).val("");
  fileInput.onchange = function(event) {
    var reader = new FileReader();
    reader.onload = function(e) {
      var contents = e.target.result;
      socket.emit('diagramUploaded', JSON.parse(contents));
    };
    reader.readAsText(fileInput.files[0]);
  }
  fileInput.click();
});

if (!window.FileList || !window.FileReader) {
  window.alert(
    'Looks like you use an older browser that does not support drag and drop. ' +
    'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
  registerFileDrop(document, (xml) => {
    socket.emit('diagramLoaded', xml);
  });
}

function processDiagram(data) {
  var canvasNode = document.getElementById("canvas");
  canvasNode.innerHTML = '';
  var viewer = new BpmnViewer({ container: document.getElementById("canvas"),
                                moddleExtensions: {
                                  be: bePackage
                                }});

  clientSemantics = new BPMNClient();
  render = (canvas, state) => {
    for (let element of state.allElements) {
      removeMarkers(element, canvas);
    }
    for (let element of state.executed) {
      canvas.addMarker(element, 'executed');
    }
    for (let element of state.enabled){
      canvas.addMarker(element, 'enabled');
    }
    for (let pair of state.selectable) {
      for(let element of pair) {
        canvas.addMarker(element, 'selectable');
      }
    }
    for (let element of state.deselectable) {
      canvas.addMarker(element, 'selectable');
    }
    for (let element of state.blocked) {
      canvas.addMarker(element, 'blocked');
    }
  };

  destroyEditors();
  var xml = data.data;
  clientSemantics.currentState = data.state;

  // Set the sequnce flows as map
  var seqflows = JSON.parse(data.flows);
  for(let pair of seqflows) {
    clientSemantics.sequenceFlows.set(pair[0], pair[1]);
  }

  var moddle = new BpmnModdle([bePackage]);

  viewer.importXML(xml, (err) => {
     if (!err) {
       canvas = viewer.get('canvas');
       var elementRegistry = viewer.get('elementRegistry');

       render(canvas, clientSemantics.currentState);

       var defineTaskSchemaButton = document.getElementById('define-task-schema');
       $(defineTaskSchemaButton).unbind();
       defineTaskSchemaButton.disabled = false;

       var editor = new SimulationEditor();

       var defineTaskSchemaButton = document.getElementById('define-task-schema');
       $(defineTaskSchemaButton).unbind();
       defineTaskSchemaButton.disabled = false;

       var startButton = document.getElementById('start-simulation');
       $(startButton).unbind();
       startButton.disabled = false;
       $(startButton).bind('click', function() {
         startSimulation(startButton, defineTaskSchemaButton, clientSemantics, editor);
         socket.emit('action', {type: 'SIMULATION_STARTED'});
         socket.emit('businessObject', {schema: window.businessDataObject.getEditor('root').getValue()});
       });

       $(defineTaskSchemaButton).bind('click', function() {
         window.openTaskSchemaForm(editor.selectPropertiesFromSchema);
       });

       var sendBugButton = document.getElementById('send-feedback');
       $(sendBugButton).unbind();
       $(sendBugButton).bind('click', function() {
         window.openBugWindow(xml);
       });

       var downloadDiagramButton = document.getElementById('download-diagram');
       $(downloadDiagramButton).unbind();
       $(downloadDiagramButton).bind('click', function() {
        socket.emit('askForData');
       });

       window.setTaskSchema({
         "type": "object",
         "properties": {
           "json-representation": {
             "type": "string",
             "media": {
               "type": "application/json"
             }
           }
         }
       });

       window.setScriptTaskEditor();
       window.setFlowSequenceEditor();

       // Set the default value for the task schema as an empty json object
       window.taskSchemaObject.getEditor('root.json-representation').setValue(data.businessObject);
       window.taskSchema = JSON.parse(editor.selectPropertiesFromSchema(JSON.parse(window.taskSchemaObject.getEditor('root').getValue()['json-representation'])));
       window.setTaskEditor(window.taskSchema);

       window.setMessageEventEditor(window.taskSchema);

       // If Simulation not started, start it now
       if(!clientSemantics.simulationStarted && data.state.started) {
         startSimulation(startButton, defineTaskSchemaButton, clientSemantics, editor);
         window.businessDataObject.setValue(data.businessObjectData);
       }

       var eventBus = viewer.get('eventBus');
       eventBus.on('element.click', event => {
         if(clientSemantics.simulationStarted) {
           if(clientSemantics.isElementSelectable(clientSemantics.currentState, event.element.id)) {
             var elem = clientSemantics.getElement(clientSemantics.currentState, event.element.businessObject.id);
             // If it is User task, the window is different
             if(clientSemantics.isUserTask(elem)) {
               openUserTaskWindow({'state': clientSemantics.currentState, 'element': elem, deselectable: false}, editor);
             }
             // Handle Message Events
             else if(clientSemantics.isMessageEvent(elem) && !clientSemantics.isThrowEvent(elem)) {
               handleMessageEvent(elem, editor);
               socket.emit('action', {type: 'SELECT_ELEMENT', element: event.element.id, selection: 'selectable', elType: event.element.type});
             }
             // Handle Script Tasks
             else if(clientSemantics.isScriptTask(elem)) {
               handleScriptTask(elem, socket);
               socket.emit('action', {type: 'SELECT_ELEMENT', element: event.element.id, selection: 'selectable', elType: event.element.type});
             }
             else {
               socket.emit('action', {type: 'SELECT_ELEMENT', element: event.element.id, selection: 'selectable', elType: event.element.type});
             }
           }
           else if(clientSemantics.isElementDeselectable(clientSemantics.currentState, event.element.id)) {
             var elem = clientSemantics.getElement(clientSemantics.currentState, event.element.businessObject.id);
             // If it is User task, the window is different
             if(clientSemantics.isUserTask(elem)) {
               openUserTaskWindow({'state': clientSemantics.currentState, 'element': elem, deselectable: true}, editor);
             }
             else {
               window.openEnablementWindow({element: event.element.businessObject, state: clientSemantics.currentState});
             }
           }
           else {
             // if the next enabled element is a user task
             if(clientSemantics.currentState.enabled.length > 0 &&
                clientSemantics.isUserTask(clientSemantics.currentState.enabled[0])) {
               openUserTaskWindow({'state': clientSemantics.currentState, 'element': clientSemantics.currentState.enabled[0],
                                  deselectable: false}, editor);
             }
             // Handle Script Tasks
             else if(clientSemantics.currentState.enabled.length > 0 &&
                      clientSemantics.isScriptTask(clientSemantics.currentState.enabled[0])) {
                var elem = clientSemantics.currentState.enabled[0];
                handleScriptTask(elem, socket);
                socket.emit('action', {type: 'FIRE_ELEMENT'});
             }
             else {
                socket.emit('action', {type: 'FIRE_ELEMENT'});
             }
           }
         }
         else {
           if(clientSemantics.isUserTask(event.element.businessObject)) {
             var element = clientSemantics.getElement(clientSemantics.currentState, event.element.businessObject.id);
             window.openTaskForm(element, moddle);
           }
           else if(clientSemantics.isMessageEvent(event.element.businessObject)) {
             var element = clientSemantics.getElement(clientSemantics.currentState, event.element.businessObject.id);
             var messageExt = getExtension(element, 'be:MessageEvent');
             window.messageEventEditor.getEditor('root').setValue(JSON.parse(messageExt.message));
             window.openMessageEventForm(element, moddle);
           }
           else if(clientSemantics.isScriptTask(event.element.businessObject)) {
             var element = clientSemantics.getElement(clientSemantics.currentState, event.element.businessObject.id);
             var scriptExt = getExtension(element, 'be:ScriptTask');
             window.scriptTaskEditor.getEditor('root').setValue(JSON.parse(scriptExt.script));
             window.openScriptTaskForm(element, moddle);
           }
           else if(clientSemantics.isSequenceFlow(event.element.businessObject)) {
             var element = clientSemantics.sequenceFlows.get(event.element.businessObject.id);
             var conditionExt = getExtension(element, 'be:FlowCondition');
             window.flowSequnceEditor.getEditor('root').setValue(JSON.parse(conditionExt.condition));
             window.openFlowConditionForm(element, moddle);
           }
         }
       });

       $(window).unbind();

       $(window).bind('newTaskSchemaObject', (event) => {
         redefineTaskForm(clientSemantics);
         // Emit the schema to the server
         socket.emit('dataObject', {schema: window.taskSchemaObject.getEditor('root').getValue()['json-representation']});
       });

       $(window).bind('extensionSet', (event) => {
         // Just send the state containing the update element to the server
         socket.emit('action', {type: 'UPDATE_ELEMENT', extension: event.detail.schema, elementId: event.detail.id});
       });

       $(window).bind('flowConditionSet', (event) => {
         socket.emit('defineCondition', {extension: event.detail.schema, elementId: event.detail.id});
       });

       $(window).bind('proceedSimulation', (event) => {
        socket.emit('action', {type: 'PROCEED_ELEMENT', state: event.detail.state,
                                element: event.detail.element});
        socket.emit('businessObject', {schema: window.businessDataObject.getEditor('root').getValue()});
       });

       $(window).bind('eventExecuted', () => {
         socket.emit('businessObject', {schema: window.businessDataObject.getEditor('root').getValue()});
       });

       $(window).bind('enableElement', (event) => {
         var elem = clientSemantics.getElement(clientSemantics.currentState, event.detail.element.id);
         if(clientSemantics.isMessageEvent(elem) && !clientSemantics.isThrowEvent(elem)) {
           handleMessageEvent(elem, editor);
           socket.emit('action', {type: 'SELECT_ELEMENT', element: event.detail.element.id,
                                   selection: 'deselectable', elType: event.detail.element.$type});
         }
         // Handle Script Tasks
         else if(clientSemantics.isScriptTask(elem)) {
           handleScriptTask(elem, socket);
           socket.emit('action', {type: 'SELECT_ELEMENT', element: event.detail.element.id,
                                   selection: 'deselectable', elType: event.detail.element.$type});
         }
         else {
           socket.emit('action', {type: 'SELECT_ELEMENT', element: event.detail.element.id,
                                   selection: 'deselectable', elType: event.detail.element.$type});
         }
       });

       $(window).bind('disableElement', (event) => {
        socket.emit('action', {type: 'DISABLE_ELEMENT', state: event.detail.state,
                                element: event.detail.element.id});
       });

       if(socket.initialization) {
         socket.on('state', msg => {
            clientSemantics.currentState = msg.state;
            // If Simulation not started, start it now
            if(!clientSemantics.simulationStarted && msg.state.started) {
              startSimulation(startButton, defineTaskSchemaButton, clientSemantics, editor);
            }
            render(canvas, msg.state);
         });

         socket.on('dataObject', data => {
           window.taskSchemaObject.destroy();
           window.setTaskSchema({
             "type": "object",
             "properties": {
               "json-representation": {
                 "type": "string",
                 "media": {
                   "type": "application/json"
                 }
               }
             }
           });
           window.taskSchemaObject.getEditor("root.json-representation").setValue(data.schema);
           window.saveTaskSchemaForm(editor.selectPropertiesFromSchema);
           redefineTaskForm(clientSemantics);
         });

         socket.on('businessObject', data => {
           window.businessDataObject.setValue(data.schema);
         });

         socket.on('defineCondition', data => {
           clientSemantics.sequenceFlows.set(data.id, data);
         });

         socket.on('sendData', data => {
           data.diagram = xml;
           var a = document.getElementById("a");
           var blob = new Blob([JSON.stringify(data)], {type: "application/json"});
           a.href = URL.createObjectURL(blob);
           a.download = "diagram.lbee";
           a.click();
         });

         socket.initialization = false;
       }
     }
  });
}
