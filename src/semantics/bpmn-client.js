class BPMNClient {
  constructor () {
    this.currentState = null;
    this.simulationStarted = false;
    this.sequenceFlows = new Map();
  }

  isElementSelectable(state, element) {
    for(let pair of state.selectable) {
      for(let sel of pair) {
        // Sometimes the label of the element is sent instead of the element
        element = element.replace('_label', '');
        if(sel.id === element) {
          return true;
        }
      }
    }

    return false;
  }

  isElementDeselectable(state, element) {
    for (let des of state.deselectable) {
      // Sometimes the label of the element is sent instead of the element
      element = element.replace('_label', '');
      if(des.id === element) {
        return true;
      }
    }
    return false;
  }

  isUserTask(element) {
    return element.$type.indexOf('UserTask') > -1;
  }

  isMessageEvent(element) {
    if (!element.eventDefinitions) {
      return false;
    }

    for(let def of element.eventDefinitions) {
      if(def.$type.indexOf('MessageEvent') > -1) {
        return true;
      }
    }

    return false;
  }

  isScriptTask(element) {
    return element.$type.indexOf('ScriptTask') > -1;
  }

  isThrowEvent(element) {
    return element.$type.indexOf('ThrowEvent') > -1;
  }

  isSequenceFlow(element) {
    return element.$type.indexOf('SequenceFlow') > -1;
  }

  getElement(state, element_id) {
    var elem;
    // search for the element in the list
    for(let el of state.allElements) {
      if (el.id == element_id) {
        elem = el;
      }
    }

    return elem;
  }
}

module.exports = {
  BPMNClient: BPMNClient
};
